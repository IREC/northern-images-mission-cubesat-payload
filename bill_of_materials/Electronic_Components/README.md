# Electronic Bill of Materials

## Generating Electronic BOMs

The electronic component BOMs are generated automatically using KICAD. The script selected is the default script 'bom_csv_sorted_by_ref'. This outputs a CSV file (will need to add the extension to the file). This file is then run through a converter such as (https://www.convertcsv.com/csv-to-markdown.) to generate a markdown version of the CSV file.

## Post-processing BOMs

- Depending on the final need, several coloums of the BOM can be removed for converting it to Markdown to make things more compact.

## Tips for the future:

- Edit the properties of each component in KiCAD as the component is selected. This will make the exporting of a BOM much simpler and more compact. The information that KiCAD includes by default is really challenging to format into a compact format for creating a assembly guide.