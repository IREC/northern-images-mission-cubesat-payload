# NIM Gerbers

Gerbers are provided for several versions of the NIM Payload. Rev C2 is the latest iteration.

## PC104 Main Board

- [Rev C2 PC104 Main Board](PC104/NIM_PC104_RevC2_Gerbers.zip)

## Screen Board

- [Rev B1 Screen Board](Screen/NIM_Screen_PCB_RevB1_Gerbers.zip)

## Electrical Ground Support Equipment

- [3.3V Programming Adapter Board](EGSE/RevA_Ground_Programming_Adapter_Gerbers.zip)
- ICSP Programming Adapter: TBD