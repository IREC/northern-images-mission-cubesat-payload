# Parts Ordering

- [Parts Ordering](#parts-ordering)
  - [Printed Circuit Boards](#printed-circuit-boards)
    - [PC104 Main Board](#pc104-main-board)
    - [Screen Board](#screen-board)
    - [PCB Ordering Options](#pcb-ordering-options)
  - [Electronic Components](#electronic-components)
- [Next Step](#next-step)


## Printed Circuit Boards

The NIM Payload is comprised of two custom PCBs and one off-the-shelf modified PCB. The fabrication files for both of these PCBs can be found in the [Gerbers](../Bill_Of_Materials/Gerbers/README.md). These files should be suitable for the majority of fab houses to create copies of the payload. These files were used with OSHPark and JLCPCB in the past.

### PC104 Main Board
<img src="img/PC104-PCBnew.PNG" alt= “” width="200" height="value">

### Screen Board
<img src="img/screen-PCBnew.PNG" alt= “” width="150" height="value">

### PCB Ordering Options

The following list contains some recommended ordering options for the NIM Payload:
- 1.6mm FR-4 TG155
- 4 layer for the PC104 board, 2 layer for the screen board
- LeadFree HASL
- Outer Layers: 1oz Copper Weight
- Inner Layers: 0.5oz Copper Weight
- Tented Vias

## Electronic Components

A bill of materials for the electronic parts has been created using KiCAD. 

# Next Step

With all the parts ordered, the next step is [Electronic Assembly](02_Electronic_Assembly.md)