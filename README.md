# Northern Images Mission Payload

The Northern Images Mission (NIM) payload was design for AuroraSat as part of the Canadian CubeSat Project. The goal of the NIM payload is to provide an educational outreach platform onboard a 2U CubeSat through the generation and distribution of space-inspired artwork and imagery. 

![Northern Images Mission Payload](img/NIM-Flight-Assembled.jpg)

## Usage

This repository is intended to be used by those who are interested in making their own copy of the NIM Payload. To folders and files contained with in this repository cover the steps needed for manufacturing, assembling, programming, and operating a copy of the NIM Payload. 

# Getting Started

Before ordering any components, it is recommended that the interface documentation be reviewed for possible challenges or required modifications for integrating the NIM payload into a new system. 

## 01 - Interface Documentation

The Mechanical and Electrical interfaces of the payload are summarised in the [Payload Interface Control Document](Interface-Documentation/Mechanical_and_Electrical_Interface.md). The mechanical design assumes that the payload will be integrated into a CubeSat following the Open CubeSat Platform (OCP) design. THe payload is optimized to be RAM facing and be orientated so that the camera is pointing forward and slightly downward to the direction of flight.

The Software interface of the payload is summarized in the [Payload Software Interface Document](Interface-Documentation/Software_Interface.md). The software for the firmware was created with the intent of having a simple ASCII based interface with another computer onboard the spacecraft using a UART connection.

## 02 - Build Guide

The files in the build guide directory provide a step by step approach to purchasing, manufacturing, and assembling the NIM Payload. The electronic design of the payload was influenced by the intent to hand assemble all the components, for this reason there is no data provided regarding how to get the payload assembled by a third-party service or by using a reflow oven. All components of the payload can be bought from third-party suppliers with the screen assembly and the installation of helicoils being the only other process aside from the electronic assembly that is required that the builder do in house. These processes may be able to be outsourced but no information is provided to that effect.

To start the process of building a copy of the NIM payload check out the [Part Ordering Guide](Build_Guide/01_Parts_Ordering.md)

# Authors

This payload was designed by Staff of the Aurora Research Institute and Coaches from the Inubik Robotics and Engineering Club.